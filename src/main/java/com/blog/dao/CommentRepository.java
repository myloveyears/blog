package com.blog.dao;

import com.blog.po.Comment;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author hengxin
 * @version 1.0
 * @date 2020/9/14 13:51
 */
public interface CommentRepository extends JpaRepository<Comment ,Long> {

    List<Comment> findByBlogId(Long blogId, Sort sort);

    /**
     * 根据blogId查询ParentComment为null的评论列表（父级评论列表）
     * @param blogId
     * @param sort
     * @return
     */
    List<Comment> findByBlogIdAndParentCommentNull(Long blogId, Sort sort);

}
