package com.blog.service;

import com.blog.po.Comment;

import java.util.List;

/**
 * @author hengxin
 * @version 1.0
 * @date 2020/9/14 13:11
 */
public interface CommentService {

    List<Comment> listCommentByBlogId(Long blogId);

    Comment saveComment(Comment comment);
}
