package com.blog.service;

import com.blog.po.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author hengxin
 * @version 1.0
 * @date 2020/9/8 17:11
 */
public interface TagService {

    //新增
    Tag saveType(Tag tag);
    //查询
    Tag getType(Long id);

    Tag getTypeByName(String name);

    //分页
    Page<Tag> listType(Pageable pageable);

    List<Tag> listTag();

    List<Tag> listTag(String ids);

    List<Tag> listTagTop(Integer size);


    //更新
    Tag undateType(Long id,Tag tag);
    //删除
    void deleteType(Long id);
}
