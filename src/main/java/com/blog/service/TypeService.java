package com.blog.service;

import com.blog.po.Type;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author hengxin
 * @version 1.0
 * @date 2020/8/31 11:54
 */
public interface TypeService {
    //新增
    Type saveType(Type type);
    //查询
    Type getType(Long id);

    Type getTypeByName(String name);

    //分页
    Page<Type> listType(Pageable pageable);

    List<Type> listType();

    List<Type> listTypeTod(Integer size);

    //更新
    Type undateType(Long id,Type type);
    //删除
    void deleteType(Long id);

}
